import java.util.*;

import javax.swing.JOptionPane;
public class InicArreglo {
	public static void main(String[] args)
	{
		// declara la variable arreglo y la inicializa con un objeto arreglo
		System.out.println("Introduzca un par de Numeros");
		
		int[] arregloEntero = new int[3]; // crea el objeto arreglo
		String[] arregloCadenas= {"Pedro","Luis","juan","Petra"};
		double[] arregloDecimales = new double[5];
		Scanner datoEntrada = new Scanner(System.in); 
		
		for (int contador2 = 0; contador2 < arregloEntero.length; contador2++)
			arregloEntero[contador2] = datoEntrada.nextInt();
		
		System.out.printf("%s%8s%n", "Indice", "Valor"); // encabezados de columnas		
		
		// imprime el valor de cada elemento del arreglo
		for (int contador = 0; contador < arregloEntero.length; contador++)
		System.out.printf("%5d%8d%n", contador, arregloEntero[contador]);
		
		datoEntrada.close();
		
		System.out.println("\n");
		for(String cadena:arregloCadenas) {
			System.out.println(cadena);
		}
		
		
		
	}
}
